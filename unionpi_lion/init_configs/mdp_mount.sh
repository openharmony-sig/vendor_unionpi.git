#!/bin/sh

insmod /usr/lib/modules/ei_osal.ko
insmod /usr/lib/modules/ei_mbase.ko
insmod /usr/lib/modules/ei_vbuf.ko
insmod /usr/lib/modules/ei_mlink.ko
insmod /usr/lib/modules/ei_mfake.ko
insmod /usr/lib/modules/ei_region.ko
insmod /usr/lib/modules/ei_adec.ko
insmod /usr/lib/modules/ei_aenc.ko
insmod /usr/lib/modules/ei_audiocommon.ko
insmod /usr/lib/modules/ei_audioin.ko
insmod /usr/lib/modules/ei_audioout.ko
insmod /usr/lib/modules/ei_viss.ko
insmod /usr/lib/modules/ei_vgss.ko
insmod /usr/lib/modules/ei_isp.ko
insmod /usr/lib/modules/ei_vc.ko
insmod /usr/lib/modules/ei_doss.ko
insmod /usr/lib/modules/ei_axnu.ko
insmod /usr/lib/modules/ei_axvu.ko

